# Statistical Graphics
======================

## Project Title: When science goes viral! 

The authors[1] showed in their bibliometric study that the capabilities of modern science to respond to an emerging health crisis, and most likely, to any other future threats. It was surprising to see that over 3400 manuscripts were published within the first three months of the outbreak of COVID-19. From Figure 1, we see that Chinese, US, UK, and Italian researchers were the most active in the considered period, although journal papers and preprints were contributed by corresponding authors affiliated in over 60 and 50 countries, respectively. The influx of these publications related to COVID-19 motivated me to analyze the results of the article [2] using statistical visualization techniques. The authors attempted to examine the link between human mobility and the number of coronavirus disease 2019 (COVID-19) infected people in 144 countries. Since the response variable was highly skewed, we have applied multiple linear regression methods aided with log, and square root transformation in this project. We have also reproduced the results of the article [2] using a negative Binomial regression method. We found that a negative Binomial regression model failed to fit the COVID-19  data. We intentionally limited our focus on visualization only to align this project with our Statistical Graphics course. 

![Article Graph](/Figures/art.png)

## [[R Code](Notebooks/ReportStatGraphics.Rmd)]
## [[Report](Report/StatGraphicsReport.pdf)]
## [[Presentation](Presentation/FinalGraphicsPresentation.pptx)]


#### References
1.	Nowakowska et al., 2020. When science goes viral: the research response during three months of the COVID-19 outbreak Biomed. Pharmacother., 129 (2020), Article 110451, 10.1016/j.biopha.2020.110451
2.	Oztig, L.I., Askin, O.E. 2020. Human mobility and coronavirus disease 2019 (COVID-19): a negative binomial regression analysis. Public Health. 185, 364-367. 
3.	https://www.sciencedirect.com/science/article/pii/S0753332220306442



## Graphics for Statistics and Data Analysis with R, 2nd Edition, CRC.
======================================================================

### Solutions to Selected Exercises
===================================

#### Solutions for Chapter 2 [[View R Code](Notebooks/MATH6490HW___2.Rmd)]
#### Solutions for Chapter 3 [[View R Code](Notebooks/MATH6490HW3.Rmd)]
#### Solutions for Chapter 4 [[View R Code](Notebooks/HW4.Rmd)]
#### Solutions for Chapter 5 [[View R Code](Notebooks/HW5.Rmd)]
#### Solutions for Chapter 6 [[View R Code](Notebooks/HW6Jishan.Rmd )]
#### Solutions for Chapter 7 [[View R Code](Notebooks/HW7Jishan.Rmd)]
#### Solutions for Chapter 8 & 9 [[View R Code](Notebooks/HW8cloud.Rmd)]
#### Solutions for Chapter 10 [[View R Code](Notebooks/HW9.Rmd)]
#### Solutions for Chapter 12 [[View R Code](Notebooks/HW10.Rmd)]

